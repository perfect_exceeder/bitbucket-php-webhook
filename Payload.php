<?php
/**
 * Payload.php
 * Created by h8every1 on 20.04.2017 0:01
 */

namespace perfect_exceeder\Bitbucket;


class Payload
{
    private static $payload = null;

    public static function fetch()
    {
        if (self::$payload === null) {

            if (isset($_POST['payload'])) { // old method
                $PAYLOAD = $_POST['payload'];
            } else { // new method
                $PAYLOAD = json_decode(file_get_contents('php://input'));
            }

            if (empty($PAYLOAD)) {
                throw new \ErrorException('Payload is empty', 100);
            }

            if ( ! isset($PAYLOAD->repository->name, $PAYLOAD->push->changes)) {
                throw new \ErrorException('Invalid payload data', 200);
            }

            self::setPayload($PAYLOAD);
        }

        return self::getPayload();

    }

    /**
     * @return null
     */
    private static function getPayload()
    {
        return self::$payload;
    }

    /**
     * @param null $payload
     */
    private static function setPayload($payload)
    {
        self::$payload = $payload;
    }
}