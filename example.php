<?php
/**
 * example.php
 *
 * Example script for Deployer (@link https://deployer.org/)
 *
 */

use perfect_exceeder\Bitbucket\Payload;

require_once('./Payload.php');

echo '<pre>';
try {

    // path to **FOLDER** with Deployer's `deploy.php` recipe file
    $deployerPath = '/var/deployer';

    // On production site uncomment next line..
    //$bitbucketData = Payload::fetch();

    // .. and comment out the next one
    $bitbucketData = json_decode(file_get_contents('./examples/payload.json'));

    // Accept only pushes from certain repos
    $acceptedRepos = [
        "perfect_exceeder/test-repo",
        "perfect_exceeder/test-repo2",
    ];

    if ( ! in_array($bitbucketData->repository->full_name, $acceptedRepos)) {
        throw new \ErrorException('Repository "' . $bitbucketData->repository->name . '" is not supported.');
    }


    $changes = $bitbucketData->push->changes;

    if ( ! is_array($changes)) {
        throw new \ErrorException('There are no changes in commit?!');
    }

    $changes = $changes[0]->new;

    if ($changes->type != 'branch') {
        throw new \ErrorException('Only pushes for branches are accepted. ' . $changes->type . ' is not accepted.');
    }

    $cmd          = 'cd ' . $deployerPath . ' && dep deploy ';
    $deployServer = false;

    // Check which repo the commit was pushed to
    if ($bitbucketData->repository->name === 'test-repo') {

        // check which branch the commit was pushed to
        switch ($changes->name) {
            case 'master':
                $deployServer = 'site-production';
                break;
            case 'staging':
                $deployServer = 'site-staging';
                break;
        }
    } else if ($bitbucketData->repository->name == 'test-repo2') {
        $deployServer = 'site2--production';
    }

    if ( ! $deployServer) {
        throw new \ErrorException('Couldn\'t select server for deployment.');
    }

    $cmd .= $deployServer;
    system($cmd, $status);

    if ($status !== 0) {
        throw new \ErrorException('Errors while running Deployer script');
    }

} catch (\Exception $e) {
    echo $e->getMessage();
    file_put_contents('log.log', $e->getMessage());
}

echo '</pre>';